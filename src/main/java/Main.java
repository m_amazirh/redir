import core.*;
import okhttp3.HttpUrl;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.utils.URIUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) throws URISyntaxException, IOException {
        boolean localTesting = false;
        boolean verbose = false;
        String domainToReplace = "portail-convergence.ppalm.fr";

        System.setProperty("http.proxyHost","srvproxy.appli");
        System.setProperty("http.proxyPort", String.valueOf(8080));
        System.setProperty("http.proxyUser", "");
        System.setProperty("http.proxyPassword", "");;

        //List<Integer> ruleIds = FileUtils.readLines(new File("c:\\temp\\ko.txt"),"utf-8").stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
        List<Integer> ruleIds = null;



        List<Rule> rules = new RulesLoader(new FileInputStream(new File("C:\\priips\\2018_05_30\\products.xlsx")),
                0, 1, 2, 1, 66).load();

        int temporaryRedirectsCount = 0;
        int incorretStatusCodeCount = 0;
        int incorrectFinalUriCount = 0;

        for(Rule rule : rules){
            RuleCheck check;
            if(localTesting){
                check = new RuleCheck(createRuleForLocalTesting(rule, domainToReplace));
            }else{
                check = new RuleCheck(rule);
            }

            if(ruleIds == null || ruleIds.contains(rule.getId())) {
                check.check();
                if (check.isOK()) {
                    System.out.println("Rule " + rule.getId() + " is OK");
                } else {
                    System.out.print("Rule " + rule.getId() + " is KO => Reason : ");
                    if (check.hasTemporaryRedirect()) {
                        System.out.println(" Temporary redirect found");
                        temporaryRedirectsCount++;
                    } else if (!check.isFinalStatusCodeCorrect()) {
                        System.out.println(" Status code incorrect -> " + check.getLastStep().getStatusCode() + ", source url : " + check.getRule().getSourceUrl());
                        incorretStatusCodeCount++;
                    } else if (!check.isFinalUriCorrect()) {
                        System.out.println(" final uri incorrect -> was expecting '" + check.getRule().getTargetUrl() + "' but found '" + check.getLastStep().getUrl() + "'");
                        incorrectFinalUriCount++;
                    }
                }

                if(verbose && !check.isOK()){
                    System.out.println(" ");
                    System.out.println("Source = " + URLDecoder.decode(check.getRule().getSourceUrl().toString(), "utf-8"));
                    if(check.getRule().getTargetUrl() != null) {
                        System.out.println("Target = " + URLDecoder.decode(check.getRule().getTargetUrl().toString(), "utf-8"));
                    }
                    System.out.println(" ");
                }
            }
        }

        System.out.println("Incorrect Status Codes : " + incorretStatusCodeCount);
        System.out.println("Incorrect final uri : " + incorrectFinalUriCount);
        System.out.println("Incorrect temporary redirect : " + temporaryRedirectsCount);
        System.out.println("Total : " + rules.size());


    }

    private static Rule createRuleForLocalTesting(Rule rule, String domainToReplace) throws URISyntaxException {
        HttpUrl sourceUrl = rule.getSourceUrl();
        HttpUrl targetUrl = rule.getTargetUrl();

        if(targetUrl != null && sourceUrl.host().equalsIgnoreCase(targetUrl.host())){
            targetUrl = targetUrl.newBuilder().scheme("http").host("st00805").build();
        }

        sourceUrl = sourceUrl.newBuilder().scheme("http").host("st00805").build();

        if(rule instanceof RedirectRule){

            return new RedirectRule(sourceUrl, targetUrl, rule.getStatusCode(), rule.getId());
        }else{
            return new GoneRule(sourceUrl, rule.getStatusCode(), rule.getId());
        }
    }
}
