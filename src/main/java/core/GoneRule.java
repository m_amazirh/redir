package core;

import okhttp3.HttpUrl;


public class GoneRule implements Rule {
    private HttpUrl sourceUrl;
    private Integer statusCode;
    private Integer id;

    public GoneRule(HttpUrl sourceUrl, Integer statusCode, Integer id) {
        this.sourceUrl = sourceUrl;
        this.statusCode = statusCode;
        this.id = id;
    }

    public HttpUrl getSourceUrl() {
        return sourceUrl;
    }

    public HttpUrl getTargetUrl() {
        return null;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public Integer getId() {
        return id;
    }


    public void setSourceUrl(HttpUrl sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public void setTargetUrl(HttpUrl HttpUrl) {
        //do nothing
    }
}
