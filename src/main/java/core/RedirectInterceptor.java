package core;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RedirectInterceptor implements Interceptor {
    List<RequestStep> requestSteps = null;

    public RedirectInterceptor(List<RequestStep> requestSteps) {
        this.requestSteps = requestSteps;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if(response.code() == 301 || response.code() == 302){
            requestSteps.add(new RequestStep(response.code(), HttpUrl.parse(response.header("Location"))));
        }else{
            requestSteps.add(new RequestStep(response.code(), chain.request().url()));
        }
        return response;
    }
}
