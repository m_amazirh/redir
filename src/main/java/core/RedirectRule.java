package core;


import okhttp3.HttpUrl;

public class RedirectRule implements Rule {
    private HttpUrl sourceUrl;
    private HttpUrl targetUrl;
    private Integer statusCode;
    private Integer id;

    public RedirectRule(HttpUrl sourceUrl, HttpUrl targetUrl, Integer statusCode, Integer id) {
        this.sourceUrl = sourceUrl;
        this.targetUrl = targetUrl;
        this.statusCode = statusCode;
        this.id = id;
    }

    public HttpUrl getSourceUrl() {
        return sourceUrl;
    }

    public HttpUrl getTargetUrl() {
        return targetUrl;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public Integer getId() {
        return id;
    }

}
