package core;

import okhttp3.*;
import okhttp3.Authenticator;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.poi.util.StringUtil;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RequestRunner {
    private HttpUrl url;
    private List<RequestStep> steps = new ArrayList<RequestStep>();

    public RequestRunner(HttpUrl url) {
        this.url = url;
    }


    public void run() throws IOException {
        getHttpClient().newCall(new Request.Builder().url(url).build()).execute().close();
    }


    private OkHttpClient getHttpClient() {
        String proxyHost = System.getProperty("http.proxyHost");
        int proxyPort = System.getProperty("http.proxyPort") != null ? Integer.parseInt(System.getProperty("http.proxyPort")) : -1;
        String proxyUser = System.getProperty("http.proxyUser");
        String proxyPassword = System.getProperty("http.proxyPassword");;

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addNetworkInterceptor(new RedirectInterceptor(steps));
        builder.connectTimeout(10, TimeUnit.MINUTES);
        builder.readTimeout(10, TimeUnit.MINUTES);
        builder.writeTimeout(10, TimeUnit.MINUTES);

        if(proxyHost != null) {
            builder.proxySelector(proxySelector);
            if(proxyUser != null){
                Authenticator proxyAuthenticator = new Authenticator() {
                    @Override public Request authenticate(Route route, Response response) throws IOException {
                        String credential = Credentials.basic(proxyUser, proxyPassword);
                        return response.request().newBuilder()
                                .header("Proxy-Authorization", credential)
                                .build();
                    }
                };

                builder.proxyAuthenticator(proxyAuthenticator);
            }
        }

        return builder.build();
    }

    public List<RequestStep> getSteps() {
        return steps;
    }

    public RequestStep getLastStep(){
        if(steps == null || steps.size() == 0){
            return null;
        }

        return steps.get(steps.size()-1);
    }

    final ProxySelector proxySelector = new ProxySelector() {
        @Override
        public java.util.List<Proxy> select(final URI uri) {
            final List<Proxy> proxyList = new ArrayList<Proxy>(1);

            // Host
            final String host = uri.getHost();

            // Is an internal host
            if (host.startsWith("127.0.0.1") || host.startsWith("st00805")) {
                proxyList.add(Proxy.NO_PROXY);
            } else if(System.getProperty("http.proxyHost") != null && System.getProperty("http.proxyPort") != null){
                // Add proxy
                proxyList.add(new Proxy(Proxy.Type.HTTP,
                        new InetSocketAddress(System.getProperty("http.proxyHost"), Integer.parseInt(System.getProperty("http.proxyPort")))));
            }

            return proxyList;
        }

        @Override
        public void connectFailed(URI arg0, SocketAddress arg1, IOException arg2) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    };
}
