package core;

import okhttp3.HttpUrl;

public class RequestStep {
    private Integer statusCode;
    private HttpUrl url;

    public RequestStep(Integer statusCode, HttpUrl url) {
        this.statusCode = statusCode;
        this.url = url;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public HttpUrl getUrl() {
        return url;
    }
}
