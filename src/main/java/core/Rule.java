package core;


import okhttp3.HttpUrl;

public interface Rule {
    public HttpUrl getSourceUrl();
    public HttpUrl getTargetUrl();
    public Integer getStatusCode();
    public Integer getId();
}
