package core;

import okhttp3.HttpUrl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class RuleCheck {
    private Rule rule;
    private List<RequestStep> steps;

    public RuleCheck(Rule rule) {
        this.rule = rule;
    }

    public void check() throws IOException, URISyntaxException {
        HttpUrl sourceUri = rule.getSourceUrl();

        RequestRunner requestRunner = new RequestRunner(sourceUri);
        try{
        requestRunner.run();

        }catch(IOException e){
            System.out.println(" rule => " + rule.getId() + ", "+ rule.getSourceUrl());
            throw e;
        }

        steps = requestRunner.getSteps();
    }

    public List<RequestStep> getSteps() {
        return steps;
    }

    public RequestStep getLastStep(){
        if(steps == null || steps.size() == 0){
            return null;
        }

        return steps.get(steps.size()-1);
    }

    public boolean hasTemporaryRedirect(){
        for(RequestStep step : steps){
            if(step.getStatusCode() == 302){
                return true;
            }
        }
        return false;
    }


    public boolean isFinalStatusCodeCorrect(){
        if(rule instanceof RedirectRule){
            return getLastStep().getStatusCode() == 200;
        }
        return rule.getStatusCode().equals(getLastStep().getStatusCode());
    }

    public boolean isFinalUriCorrect(){
        return rule.getTargetUrl().equals(getLastStep().getUrl());
    }

    public boolean isOK(){
        if(rule.getStatusCode() == 301){
            return isFinalStatusCodeCorrect() && isFinalUriCorrect() && !hasTemporaryRedirect();
        }else{
            return isFinalStatusCodeCorrect() && !hasTemporaryRedirect();
        }
    }

    public Rule getRule() {
        return rule;
    }
}
