package core;

import okhttp3.HttpUrl;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class RulesLoader {
    private InputStream excelStream;
    private String sheetName;
    private int sourceColumn;
    private int targetColumn;
    private int statusCodeColumn;
    private int startLine;
    private int endLine;

    public RulesLoader(InputStream excelStream, int sourceColumn, int targetColumn, int statusCodeColumn, int startLine, int endLine) {
        this.excelStream = excelStream;
        this.sourceColumn = sourceColumn;
        this.targetColumn = targetColumn;
        this.statusCodeColumn = statusCodeColumn;
        this.startLine = startLine;
        this.endLine = endLine;
    }

    public List<Rule> load() throws IOException, URISyntaxException {
        List<Rule> rules = new ArrayList<Rule>();
        Workbook wb = new XSSFWorkbook(excelStream);
        Sheet sheet = wb.getSheetAt(0);

        for(int lineIdx = startLine-1; lineIdx <= endLine-1; lineIdx++){
            Row row = sheet.getRow(lineIdx);
            Rule rule;

            try {
                rule = loadRule(row, lineIdx);
            }catch(IOException | URISyntaxException e){
                System.out.println("Erreur sur la ligne : " + lineIdx);
                throw e;
            }

            if(rule!= null){
                rules.add(rule);
            }else{
                System.out.println("Ligne " + lineIdx + " n'a pas été chargé...");
            }
        }

        return rules;
    }

    private Rule loadRule(Row row, Integer lineIdx) throws MalformedURLException, URISyntaxException, UnsupportedEncodingException {
        String source = row.getCell(sourceColumn).getStringCellValue();
        String target = row.getCell(targetColumn).getStringCellValue();
        Integer statusCode = (int)row.getCell(statusCodeColumn).getNumericCellValue();
        if(source != null && statusCode != null && (statusCode == 410 || (statusCode == 301 && target != null))){
            HttpUrl sourceUri = HttpUrl.parse(source);
            if(statusCode == 301){
                HttpUrl targetUri = HttpUrl.parse(target);

                return new RedirectRule(sourceUri, targetUri, statusCode, lineIdx);
            }else if(statusCode == 410){
                return new GoneRule(sourceUri, statusCode, lineIdx);
            }
        }

        return null;
    }
}
